package com.systeric.thymeleafexplore.data;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
public class ExportEmployeeDTO {
    private String designation;
    private String subtitles;
    private String referenceDate;
    private List<Map<String,Object>> employees;
}
