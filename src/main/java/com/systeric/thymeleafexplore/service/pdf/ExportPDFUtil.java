package com.systeric.thymeleafexplore.service.pdf;

import lombok.AllArgsConstructor;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.util.Map;

@AllArgsConstructor
public class ExportPDFUtil {

    public static String createPdf(Map<String, Object> map, String template, SpringTemplateEngine templateEngine) throws Exception {
        Context ctx = new Context();
        for (Map.Entry<String, Object> stringObjectEntry : map.entrySet()) {
            Map.Entry<String, Object> pair = stringObjectEntry;
            ctx.setVariable(pair.getKey(), pair.getValue());
        }

        return templateEngine.process(template, ctx);
    }
}
