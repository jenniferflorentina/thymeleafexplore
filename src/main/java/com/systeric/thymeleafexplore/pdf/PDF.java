package com.systeric.thymeleafexplore.pdf;

import com.lowagie.text.DocumentException;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class PDF {

    static File renderPdf(String html, String pdfName) throws IOException, DocumentException {
        File file = File.createTempFile(pdfName, ".pdf");
        OutputStream outputStream = new FileOutputStream(file);
        ITextRenderer renderer = new ITextRenderer(20f * 4f / 3f, 20);
        renderer.setDocumentFromString(html);
        renderer.layout();
        renderer.createPDF(outputStream);
        outputStream.close();
        file.deleteOnExit();
        return file;
    }

    public static File htmlToPDF(String html, String pdfName) throws IOException, DocumentException {
        return PDF.renderPdf(html, pdfName);
    }
}
