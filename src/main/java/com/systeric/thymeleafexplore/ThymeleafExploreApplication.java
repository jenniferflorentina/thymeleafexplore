package com.systeric.thymeleafexplore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThymeleafExploreApplication {

	public static void main(String[] args) {
		SpringApplication.run(ThymeleafExploreApplication.class, args);
	}

}
