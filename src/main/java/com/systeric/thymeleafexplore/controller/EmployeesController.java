package com.systeric.thymeleafexplore.controller;

import com.systeric.thymeleafexplore.data.ExportEmployeeDTO;
import com.systeric.thymeleafexplore.pdf.PDF;
import com.systeric.thymeleafexplore.service.pdf.ExportPDFUtil;
import lombok.AllArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.thymeleaf.spring5.SpringTemplateEngine;

import java.io.File;
import java.util.*;

import org.apache.commons.io.FileUtils;

@Controller
@AllArgsConstructor
public class EmployeesController {
  private final SpringTemplateEngine springTemplateEngine;

  public List<Map<String,Object>> createEmployeeList() {
    List<Map<String, Object>> employees = new ArrayList<>();
    for (int i = 0; i < 5; i++) {
      Map<String, Object> map = new HashMap<>();
      map.put("First Name", "Budi");
      map.put("Last Name", "Budianto");
      map.put("Address", "Bandung");
      employees.add(map);
    }
    return employees;
  }

  @GetMapping("/employees")
  public ResponseEntity<ByteArrayResource> getEmployeesData() throws Exception {
    HttpHeaders header = new HttpHeaders();
    header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=".concat("Employee.pdf"));
    header.add("Cache-Control", "no-cache, no-store, must-revalidate");
    header.add("Pragma", "no-cache");
    header.add("Expires", "0");

    final String FILE_NAME = "Employee.pdf";
    ExportEmployeeDTO employees = new ExportEmployeeDTO("","", "2021-12-10, 14:24:48 Uhr", createEmployeeList());
    Map<String, Object> map = new HashMap<>();
    map.put("data", employees);

    String htmlString = ExportPDFUtil.createPdf(map, "exportEmployeeTemplates", springTemplateEngine);
    File pdf = PDF.htmlToPDF(htmlString, FILE_NAME);

    return ResponseEntity.ok()
            .headers(header)
            .contentLength(pdf.length())
            .contentType(MediaType.parseMediaType("application/octet-stream"))
            .body(new ByteArrayResource(FileUtils.readFileToByteArray(pdf)));
  }
}
