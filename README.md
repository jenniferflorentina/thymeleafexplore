# Thymeleaf Exploration
Here is my thymeleaf exploration with Java Spring Boot Application. \
To initialize the Spring Boot Application, simply go to [https://start.spring.io/](https://start.spring.io/). \
Generate the Spring Boot Application using this configuration. 

# Thymeleaf Documentation
For syntax and documentation Thymeleaf, go to 
- https://www.baeldung.com/thymeleaf-in-spring-mvc
- https://www.thymeleaf.org/documentation.html#introductions
